const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const cookieParser = require('cookie-parser')
const port = 3000;
require("dotenv").config();

app.use(express.json());
app.use(cors({
    origin : ['http://localhost:5173','https://65ba466151eb31d1dc0327e0--unique-heliotrope-058d10.netlify.app'],
    methods : ["POST","GET","PATCH","DELETE"],
    credentials : true,
}));
app.use(cookieParser())


const User = require("./Routes/user");
const Todo = require("./Routes/todos")


app.use("/users", User);
app.use("/todos", Todo)




app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

main()
  .then((res) => console.log("db connected"))
  .catch((err) => console.log(err));

async function main() {
  const dbUrl = process.env.DB_URL;
  const dbPassword = process.env.DB_PASSWORD;
  const UrlWithPassword = dbUrl.replace("<password>", dbPassword);
  await mongoose.connect(UrlWithPassword);
}
